package utfpr.ct.dainf.if62c.pratica;

import java.lang.Math;

/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
public class Circulo extends Elipse {
    public Circulo(double raio){
        super(raio,raio);
    }
    
    @Override
    public double getPerimetro(){
       return Math.PI * 2 * (eixo_x/2);
    }
}
